
local cpml = require('cpml')
local vec3 = cpml.vec3
local vec2 = cpml.vec2
local mat4 = cpml.mat4

local function make_class(name)
   assert(type(name) == "string", "Expected name to be a string")
   local class = {}
   class.__index = class
   class.class_name = name
   class.class_parent = nil
   local function is_an_object(obj)
      return type(obj) == "table" and obj.class_name ~= nil
   end
   function class:_set_object(obj)
      setmetatable(obj, class)
   end
   function class:is_object(obj)
      function recursive_check(obj)
         if obj.class_name == name then
            return true
         end
         if obj.class_parent ~= nil then
            return recursive_check(obj.class_parent)
         end
         return false
      end
      if not is_an_object(obj) then
         return false
      end
      
      return recursive_check(obj)
   end
   function class:assert_object(obj, assert_message)
      if is_an_object(obj) then
         assert_message = assert_message or 
            string.format("Expected class \"%s\" but instead got class \"%s\"" ,
                           name, obj.class_name)
         assert(self:is_object(obj), assert_message)
      else
         assert_message = assert_message or 
            string.format("Expected class \"%s\" but instead got \"%s\"" ,
                           name, type(obj))
            assert(false, assert_message)
      end
   end
   function class:assert_object_if_not_nil(obj, assert_message)
      if obj ~= nil then
         self:assert_object(obj, assert_message)
      end
   end
   return class
end



-- vx, vy, vz, nx, ny, nv, u, v
-- {vx, vy, vz, nx, ny, nv, u, v}
-- {vx, vy, vz}, {nx, ny, nz}, {u, v}
-- {{vx, vy, vz}, {nx, ny, nz}, {u, v}}
-- {x=vx, y=vy, z=vz}, {x=nx, y=ny, z=nz}, {x=u, y=v}
-- {{x=vx, y=vy, z=vz}, {x=nx, y=ny, z=nz}, {x=u, y=v}}
-- {position={x=vx, y=vy, z=vz}, normal={x=nx, y=ny, z=nz}, uv={x=u, y=v}}
local vertex_args_parser = (function()
   local data_names = {'v', 'n', 'uv'}
   local f =  string.format
   local function pop1(args)
      table.remove(args, 1)
   end
   local function parse_numeric_components_vec3(obj, key ,args)
      local x = args[1]
      local y = 0
      local z = 0
      pop1(args)
      assert(type(args[1]) == 'number' or type(args[1]) == 'table', f("Expected parameter 2 of %s to be number or table", key))
      if type(args[1]) == 'number' then
         y = args[1]
         pop1(args)
         assert(type(args[1]) == 'number' or type(args[1]) == 'table', f("Expected parameter 3 of %s to be number or table", key))
         if type(args[1]) == 'number' then
            z = args[1]
            pop1(args)
         end
      end
      obj[key] = vec3.new(x, y, z)
   end
   local function parse_numeric_components_vec2(obj, key ,args)
      local x = args[1]
      local y = 0
      pop1(args)
      assert(type(args[1]) == 'number' or type(args[1]) == 'table', f("Expected parameter 2 of %s to be number or table", key))
      if type(args[1]) == 'number' then
         y = args[1]
         pop1(args)
      end
      obj[key] = vec2.new(x, y)
   end
   
   local function parse_component(obj, key, args, new_function, parse_function)
      assert(type(args[1]) == 'number' or type(args[1]) == 'table', f("Expected first %s argument to be either a number or table", key))
      if type(args[1]) == 'number' then
         parse_function(obj, key, args)
      elseif type(args[1]) == 'table' then
         local t = args[1]
         if t.x ~= nil then
            obj[key] = new_function(t)
            pop1(args)
         elseif type(t[1]) == 'number' then
            parse_function(obj, key, t)
            pop1(args)
         end
      end
   end

   return function(obj, ...)
      local args = table.pack(...)

      local real_args = args
      if #args == 1 then
         local first = args[1]
         assert(type(first) == 'number' or type(first) == 'table', "Expected first parameter to be a table or number")
         if type(first) == 'table' and ((first.x == nil and first.y == nil and first.z == nil) or first.position ~= nil or first.normal ~= nil or first.uv ~= nil) then
            real_args = first
         end
      end
     
      if real_args.position ~= nil or real_args.normal ~= nil or real_args.uv ~= nil then
         if real_args.position ~= nil then
            obj.position = vec3.new(real_args.position)
         else
            obj.position = vec3.new(0, 0, 0)
         end
         if real_args.normal ~= nil then
            obj.normal = vec3.new(real_args.normal)
         else
            obj.normal = vec3.new(0, 0, 0)
         end
         if real_args.uv ~= nil then
            obj.uv = vec2.new(real_args.uv)
         else
            obj.uv = vec2.new(0, 0)
         end
      else
         parse_component(obj, 'position', real_args, vec3.new, parse_numeric_components_vec3)
         if #real_args >= 1 then
            parse_component(obj, 'normal', real_args, vec3.new, parse_numeric_components_vec3)
         else 
            obj.normal = vec3.new(0, 0, 0)
         end
         if #real_args >= 1 then
            parse_component(obj, 'uv', real_args, vec2.new, parse_numeric_components_vec2)
         else
            obj.uv = vec2.new(0, 0)
         end
      end
      assert(obj.position ~= nil or obj.normal ~=nil or obj.uv ~= nil)
      assert(vec3.is_vec3(obj.position) and vec3.is_vec3(obj.normal) and vec2.is_vec2(obj.uv))
      return obj
   end
end)()


local Vertex = make_class("Vertex")
local Edge = make_class("Edge")
local Face = make_class("Face")
local FaceEdge = make_class("FaceEdge")
local Mesh = make_class("Mesh")
local GeometrySet = make_class("GeometrySet")

function Vertex:new(...)
   local v = {
      edge = nil,
   }
   vertex_args_parser(v, ...)
   self:_set_object(v)
   return v
end

function Vertex:find_edge_with(vertex)
   Vertex:assert_object(vertex)
   local edge = self.edge
   if edge == nil then
      return nil
   end
   repeat
      if edge:has_vertex(vertex) then
         return edge
      end
      edge = edge:next(self)
   until edge == self.edge
   return nil
end

function Vertex:lerp(other, percent)
   local pos = self.position:lerp(other.position, percent)
   local normal = self.normal:lerp(other.normal, percent)
   local uv = self.uv:lerp(other.uv, percent)
   return Vertex:new(pos, normal, uv)
end

function Vertex:edge_count()
   local edge = self.edge
   if edge == nil then
      return 0
   end
   local count = 0
   repeat
      count = count + 1
      edge = edge:next(self)
   until edge == self.edge
   return count
end

function Vertex:find_face_edge(edge_or_face)
   assert(Face:is_object(edge_or_face) or Edge:is_object(edge_or_face), "Expected edge_or_face to be a Edge or Face")
   local edge = self.edge
   if edge == nil then
      return nil
   end
   repeat
      local face_edge = edge.face_edge
      if face_edge ~= nil then
         repeat
            if face_edge.vertex == self and (edge == edge_or_face or face_edge.face == edge_or_face) then
               return face_edge
            end
            face_edge = face_edge.radial_next
         until face_edge == edge.face_edge
      end
      edge = edge:next(self)
   until edge == self.edge
   return nil
end


function Vertex:to_string()
   return string.format("<%s,%s,%s>", self.position, self.normal, self.uv)
end
Vertex.__tostring = Vertex.to_string




function Edge:new(sharp)
   if sharp == nil then
      sharp = true
   end
   local e = {
      v1 = nil,
      v2 = nil,
      sharp = sharp,
      face_edge = nil,
      v1_next = nil,
      v1_prev = nil,
      v2_next = nil,
      v2_prev = nil,
   }
   self:_set_object(e)
   return e
end

function Edge:next(vertex)
   assert(self:has_vertex(vertex), "Expected vertex to be on the Edge")
   if vertex == self.v1 then
      return self.v1_next
   end
   return self.v2_next
end

function Edge:prev(vertex)
   assert(self:has_vertex(vertex), "Expected vertex to be on the Edge")
   if vertex == self.v1 then
      return self.v1_prev
   end
   return self.v2_prev
end

function Edge:next_other(vertex)
   assert(self:has_vertex(vertex), "Expected vertex to be on the Edge")
   if vertex == self.v1 then
      return self.v2_next
   end
   return self.v1_next
end

function Edge:prev_other(vertex)
   assert(self:has_vertex(vertex), "Expected vertex to be on the Edge")
   if vertex == self.v1 then
      return self.v2_prev
   end
   return self.v1_prev
end

function Edge:other_vertex(vertex)
   assert(self:has_vertex(vertex), "Expected vertex to be on the Edge")
   if vertex == self.v1 then
      return self.v2
   end
   return self.v1
end

function Edge:get_vertex(index)
   assert(type(index) == 'number', "Expected index to be a number")
   assert(index == 1 or index == 2, "Expected index to be 1 or 2")
   if index == 1 then
      return self.v1
   end
   return self.v2
end

function Edge:get_index(vertex)
   assert(self:has_vertex(vertex), "Expected vertex to be on the Edge")
   if vertex == self.v1 then
      return 1
   end
   return 2
end

function Edge:has_vertex(vertex)
   Vertex:assert_object(vertex)
   return vertex == self.v1 or vertex == self.v2
end

function Edge:has_both_vertices(v1, v2)
   Vertex:assert_object(v1)
   Vertex:assert_object(v2)
   assert(v1 ~= v2, "Expected v1 and v2 to be different")
   return (v1 == self.v1 and v2 == self.v2) or (v1 == self.v2 and v2 == self.v1)
end

function Edge:face_count()
   local face_edge = self.face_edge
   if face_edge == nil then
      return 0
   end
   local count = 0
   repeat
      count = count + 1
      face_edge = face_edge.radial_next
   until face_edge == self.face_edge
   return count
end

function Edge:find_face_edge(vertex_or_face)
   assert(Vertex:is_object(vertex_or_face) or Face:is_object(vertex_or_face), "Expected vertex_or_face to be a Vertex or Face")
   local face_edge = self.face_edge
   if face_edge == nil then
      return nil
   end
   repeat
      if face_edge.face == vertex_or_face or face_edge.vertex == vertex_or_face then
         return face_edge
      end
      face_edge = face_edge.radial_next
   until face_edge == self.face_edge
   return nil
end

function Edge:has_face(face)
   local face_edge = self:find_face_edge(face)
   return face_edge ~= nil
end

function Edge:to_string()
   return string.format("|%s,%s|", self.v1.position, self.v2.position)
end
Edge.__tostring = Edge.to_string


local edge_key_table = {
   { next = 'v1_next', prev = 'v1_prev', vertex = 'v1' },
   { next = 'v2_next', prev = 'v2_prev', vertex = 'v2' },
}


function Face:new()
   local f = {
      face_edge = nil,
   }
   self:_set_object(f)
   return f
end

function Face:find_face_edge(vertex_or_edge)
   assert(Vertex:is_object(vertex_or_edge) or Edge:is_object(vertex_or_edge), "Expected vertex_or_edge to be a Vertex or Edge")
   local face_edge = self.face_edge
   repeat
      if face_edge.vertex == vertex_or_edge or face_edge.edge == vertex_or_edge then
         return face_edge
      end
      face_edge = face_edge.face_next
   until face_edge == self.face_edge
   return nil
end

function Face:has_edge(edge)
   local face_edge = self:find_face_edge(edge)
   return face_edge ~= nil
end

function Face:has_vertex(vertex)
   local face_edge = self:find_face_edge(vertex)
   return face_edge ~= nil
end


function Face:edge_count()
   local face_edge = self.face_edge
   local count = 0
   repeat
      count = count + 1
      face_edge = face_edge.face_next
   until face_edge == self.face_edge
   return count
end

function Face:shared_edge_count(face)
   Face:assert_object(face)
   if self == face then
      return self:edge_count()
   end
   local count = 0
   local face_edge = self.face_edge
   repeat
      if face_edge.edge:has_face(face) then
         count = count + 1
      end
      face_edge = face_edge.face_next
   until face_edge == self.face_edge
   return count
end

function Face:reverse()
   local face_edge = self.face_edge
   local last_vertex = face_edge.vertex
   repeat
      local next_face_edge = face_edge.face_next
      local prev_face_edge = face_edge.face_prev
      
      face_edge.vertex = next_face_edge.vertex
      
      face_edge.face_next = prev_face_edge
      face_edge.face_prev = next_face_edge
      
      face_edge = next_face_edge
   until face_edge == self.face_edge

   self.face_edge.face_next.vertex = last_vertex
end

function Face:compute_normal()
   local normal = vec3.new(0, 0, 0)
   local face_edge = self.face_edge
   repeat
      local c = face_edge.vertex.position
      local n = face_edge.face_next.vertex.position
      
      normal.x = normal.x + ((c.y - n.y) * (c.z + n.z))
      normal.y = normal.y + ((c.z - n.z) * (c.x + n.x))
      normal.z = normal.z + ((c.x - n.x) * (c.y + n.y))
      
      face_edge = face_edge.face_next
   until face_edge == self.face_edge
   return normal:normalize()
end


function FaceEdge:new()
   local fe = {
      face = nil,
      edge = nil,
      vertex = nil,
      face_next = nil,
      face_prev = nil,
      radial_next = nil,
      radial_prev = nil,
   }
   self:_set_object(fe)
   return fe
end

function GeometrySet:new()
   local gs = {
      vertices = {},
      edges = {},
      faces = {},
      face_edges = {},
   }
   self:_set_object(gs)
   return gs
end

function GeometrySet:use_or_new(geometry_set)
   if geometry_set == nil then
      geometry_set = GeometrySet:new()
   else
      GeometrySet:assert_object(geometry_set)
   end
   return geometry_set
end


function GeometrySet:add_vertices(...)
   local args = table.pack(...)
   assert(#args >= 1, "Expected at least one parameter")
   local input_list = nil
   if Vertex:is_object(args[1]) or GeometrySet:is_object(args[1]) then
      input_list = args
   else
      assert(type(args[1]) == "table", "Expected table, Vertex, or GeometrySet")
      input_list = args[1]
   end
   
   for i, v in ipairs(input_list) do
      assert(Vertex:is_object(v) or GeometrySet:is_object(v), "Expected either Vertex or GeometrySet")
      if Vertex:is_object(v) then
         self.vertices[v] = true
      else
         for k, v2 in pairs(v.vertices) do
            if v2 then
               self.vertices[k] = true
            end
         end
      end
   end
end

function GeometrySet:remove_vertices(...)
   local args = table.pack(...)
   assert(#args >= 1, "Expected at least one parameter")
   local input_list = nil
   if Vertex:is_object(args[1]) or GeometrySet:is_object(args[1]) then
      input_list = args
   else
      assert(type(args[1]) == "table", "Expected table, Vertex, or GeometrySet")
      input_list = args[1]
   end
   for i, v in ipairs(input_list) do
      assert(Vertex:is_object(v) or GeometrySet:is_object(v), "Expected either Vertex or GeometrySet")
      if Vertex:is_object(v) then
         self.vertices[v] = nil
      else
         for k, v2 in pairs(v.vertices) do
            if v2 then
               self.vertices[k] = nil
            end
         end
      end
   end
end

function GeometrySet:get_vertices()
   local list = {}
   for k, v in pairs(self.vertices) do
      if v then
         table.insert(list, k)
      end
   end
   return list
end

function GeometrySet:add_edges(...)
   local args = table.pack(...)
   assert(#args >= 1, "Expected at least one parameter")
   local input_list = nil
   if Edge:is_object(args[1]) or GeometrySet:is_object(args[1]) then
      input_list = args
   else
      assert(type(args[1]) == "table", "Expected table, Edge, or GeometrySet")
      input_list = args[1]
   end
   
   for i, v in ipairs(input_list) do
      assert(Edge:is_object(v) or GeometrySet:is_object(v), "Expected either Edge or GeometrySet")
      if Edge:is_object(v) then
         self.edges[v] = true
      else
         for k, v2 in pairs(v.edges) do
            if v2 then
               self.edges[k] = true
            end
         end
      end
   end
end

function GeometrySet:remove_edges(...)
   local args = table.pack(...)
   assert(#args >= 1, "Expected at least one parameter")
   local input_list = nil
   if Edge:is_object(args[1]) or GeometrySet:is_object(args[1]) then
      input_list = args
   else
      assert(type(args[1]) == "table", "Expected table, Edge, or GeometrySet")
      input_list = args[1]
   end
   for i, v in ipairs(input_list) do
      assert(Edge:is_object(v) or GeometrySet:is_object(v), "Expected either Edge or GeometrySet")
      if Edge:is_object(v) then
         self.edges[v] = nil
      else
         for k, v2 in pairs(v.edges) do
            if v2 then
               self.edges[k] = nil
            end
         end
      end
   end
end

function GeometrySet:get_edges()
   local list = {}
   for k, v in pairs(self.edges) do
      if v then
         table.insert(list, k)
      end
   end
   return list
end


function GeometrySet:add_faces(...)
   local args = table.pack(...)
   assert(#args >= 1, "Expected at least one parameter")
   local input_list = nil
   if Face:is_object(args[1]) or GeometrySet:is_object(args[1]) then
      input_list = args
   else
      assert(type(args[1]) == "table", "Expected table, Face, or GeometrySet")
      input_list = args[1]
   end
   
   for i, v in ipairs(input_list) do
      assert(Face:is_object(v) or GeometrySet:is_object(v), "Expected either Face or GeometrySet")
      if Face:is_object(v) then
         self.faces[v] = true
      else
         for k, v2 in pairs(v.faces) do
            if v2 then
               self.faces[k] = true
            end
         end
      end
   end
end

function GeometrySet:remove_faces(...)
   local args = table.pack(...)
   assert(#args >= 1, "Expected at least one parameter")
   local input_list = nil
   if Face:is_object(args[1]) or GeometrySet:is_object(args[1]) then
      input_list = args
   else
      assert(type(args[1]) == "table", "Expected table, Face, or GeometrySet")
      input_list = args[1]
   end
   for i, v in ipairs(input_list) do
      assert(Face:is_object(v) or GeometrySet:is_object(v), "Expected either Face or GeometrySet")
      if Face:is_object(v) then
         self.faces[v] = nil
      else
         for k, v2 in pairs(v.faces) do
            if v2 then
               self.faces[k] = nil
            end
         end
      end
   end
end

function GeometrySet:get_faces()
   local list = {}
   for k, v in pairs(self.faces) do
      if v then
         table.insert(list, k)
      end
   end
   return list
end

function GeometrySet:vertex_count()
   local count = 0
   for k, v in pairs(self.vertices) do
      if v then
         count = count + 1
      end
   end
   return count
end

function GeometrySet:edge_count()
   local count = 0
   for k, v in pairs(self.edges) do
      if v then
         count = count + 1
      end
   end
   return count
end

function GeometrySet:face_count()
   local count = 0
   for k, v in pairs(self.faces) do
      if v then
         count = count + 1
      end
   end
   return count
end

function GeometrySet:clear_vertices()
   for k, v in pairs(self.vertices) do
      self.vertices[k] = nil
   end
end

function GeometrySet:clear_edges()
   for k, v in pairs(self.edges) do
      self.edges[k] = nil
   end
end

function GeometrySet:clear_faces()
   for k, v in pairs(self.faces) do
      self.faces[k] = nil
   end
end

function GeometrySet:clear()
   self:clear_vertices()
   self:clear_edges()
   self:clear_faces()
end

function GeometrySet:add_all(geometry_set)
   GeometrySet:assert_object(Geometry_set)
   for k, v in pairs(geometry_set.vertices) do
      if v then
         self.vertices[k] = true
      end
   end
   for k, v in pairs(geometry_set.edges) do
      if v then
         self.edges[k] = true
      end
   end
   for k, v in pairs(geometry_set.faces) do
      if v then
         self.faces[k] = true
      end
   end
end

function GeometrySet:remove_all(geometry_set)
   GeometrySet:assert_object(Geometry_set)
   for k, v in pairs(geometry_set.vertices) do
      if v then
         self.vertices[k] = nil
      end
   end
   for k, v in pairs(geometry_set.edges) do
      if v then
         self.edges[k] = nil
      end
   end
   for k, v in pairs(geometry_set.faces) do
      if v then
         self.faces[k] = nil
      end
   end
end

function GeometrySet:toggle_all(geometry_set)
   GeometrySet:assert_object(Geometry_set)
   local function toggle(t, k)
      if t[k] == true then
         t[k] = nil
      else
         t[k] = true
      end
   end
   for k, v in pairs(geometry_set.vertices) do
      if v then
         toggle(self.vertices, k)
      end
   end
   for k, v in pairs(geometry_set.edges) do
      if v then
         toggle(self.edges, k)
      end
   end
   for k, v in pairs(geometry_set.faces) do
      if v then
         toggle(self.edges, k)
      end
   end
end


function GeometrySet:copy()
   local gs = GeometrySet:new()
   gs:add_all(self)
   return gs
end

function GeometrySet:add_vertices_from_edges()
   for k, v in pairs(self.edges) do
      if v then
         self:add_vertices(k.v1, k.v2)
      end
   end
end

function GeometrySet:add_vertices_from_face()
   for k, v in pairs(self.faces) do
      if v then
         local face_edge = k.face_edge
         repeat
            self:add_vertices(face_edge.vertex)
            face_edge = face_edge.face_next
         until face_edge == k.face_edge
      end
   end
end

function GeometrySet:add_edges_from_face()
   for k, v in pairs(self.faces) do
      if v then
         local face_edge = k.face_edge
         repeat
            self:add_edges(face_edge.edge)
            face_edge = face_edge.face_next
         until face_edge == k.face_edge
      end
   end
end


function Mesh:new()
   local m = {
      vertices = {},
      edges = {},
      faces = {},
      face_edges = {},
   }
   self:_set_object(m)
   return m
end

function Mesh:clear()
   self.vertices = {}
   self.edges = {}
   self.faces = {}
   self.face_edges = {}
end

function Mesh:has_vertex(vertex)
   Vertex:assert_object(vertex)
   return self.vertices[vertex] == true
end

function Mesh:has_edge(edge)
   Edge:assert_object(edge)
   return self.edges[edge] == true
end

function Mesh:has_edge_between(v1, v2)
   assert(self:has_vertex(v1), "Expected to own v1")
   assert(self:has_vertex(v2), "Expected to own v2")
   local edge = v1:find_edge_with(v2)
   return edge ~= nil
end

function Mesh:has_face_edge(face_edge)
   FaceEdge:assert_object(face_edge)
   return self.face_edges[face_edge] == true
end

function Mesh:has_face(face)
   Face:assert_object(face)
   return self.faces[face] == true
end



-- x, y, z, nx, ny, nz, u, v
-- {x, y, z}, {nx, ny, nz}, {u, v}
function Mesh:add_vertex(...)
   local vertex = Vertex:new(...)
   self.vertices[vertex] = true
   return vertex
end

function Mesh:get_edge(v1, v2)
   Vertex:assert_object(v1)
   Vertex:assert_object(v2)
   assert(self.vertices[v1], "Expected v1 to be a part of the mesh")
   assert(self.vertices[v2], "Expected v2 to be a part of the mesh")
   assert(v1 ~= v2, "Expected v1 to be different than v2")

   local edge = v1:find_edge_with(v2)
   return edge
end

local function edge_keys_from(edge, vertex)
   return edge_key_table[edge:get_index(vertex)]
end

local function attach_edge_to_vertex(edge, vertex, v_index)
   -- Add Edge to vertex edge list
   -- Any order.
   local new_edge_keys = edge_key_table[v_index]
   edge[new_edge_keys.vertex] = vertex
   if vertex.edge == nil then
      vertex.edge = edge
      edge[new_edge_keys.next] = edge
      edge[new_edge_keys.prev] = edge
   else
      local prev_edge = vertex.edge
      local next_edge = vertex.edge:next(vertex)
      local prev_edge_keys = edge_keys_from(prev_edge, vertex)
      local next_edge_keys = edge_keys_from(next_edge, vertex)

      
      edge[new_edge_keys.next] = next_edge
      edge[new_edge_keys.prev] = prev_edge
      prev_edge[prev_edge_keys.next] = edge
      next_edge[next_edge_keys.prev] = edge
   end
end

function Mesh:add_edge(v1, v2, sharp)
   assert(not self:has_edge_between(v1, v2), "Edge already exists")
   
   local edge = Edge:new(sharp)
   attach_edge_to_vertex(edge, v1, 1) 
   attach_edge_to_vertex(edge, v2, 2)
   self.edges[edge] = true
   return edge
end

function Mesh:get_or_add_edge(v1, v2, sharp)
   local edge = self:get_edge(v1, v2)
   if edge ~= nil then
      return edge
   end
   return self:add_edge(v1, v2, sharp)
end


local function attach_face_edge_to_edge(face_edge, edge)
   face_edge.edge = edge
   if edge.face_edge == nil then
      edge.face_edge = face_edge
      face_edge.radial_next = face_edge
      face_edge.radial_prev = face_edge
   else
      local radial_prev_face_edge = edge.face_edge
      local radial_next_face_edge = edge.face_edge.radial_next
      
      face_edge.radial_next = radial_next_face_edge
      face_edge.radial_prev = radial_prev_face_edge
      radial_prev_face_edge.radial_next = face_edge
      radial_next_face_edge.radial_prev = face_edge
   end
end

-- Index List CCW = front face
function Mesh:add_face(...)
   local args = table.pack(...)
   assert(#args >= 1, "Expected at least one parameter")
   local vertex_list = args
   if type(args[1]) == 'table' and not Vertex:is_object(args[1]) then
      vertex_list = args[1]
   end
   -- Check things
   assert(#vertex_list > 2, "Expected more than 2 vertices")
   for i, vertex in ipairs(vertex_list) do
      assert(Vertex:is_object(vertex), string.format("Expected vertex %d to be a number", i))
      assert(self.vertices[vertex] == true, string.format("Expected vertex %d to be a valid index", i))
   end
   
   -- TODO: Assert Face doesn't already exist (Different windings are ok)

   local face = Face:new()
   local prev_vertex = vertex_list[#vertex_list]
   local prev_face_edge = nil
   local first_face_edge = nil
   for i, vertex in ipairs(vertex_list) do
      local edge = self:get_or_add_edge(prev_vertex, vertex)
      local face_edge = FaceEdge:new()
      face_edge.vertex = prev_vertex
      
      -- Attach edge and face_edge together
      attach_face_edge_to_edge(face_edge, edge)
      
      -- Attach to prev_face_edge
      if prev_face_edge ~= nil then
         prev_face_edge.face_next = face_edge
         face_edge.face_prev = prev_face_edge
      end
      
      -- Attach to face
      face_edge.face = face
      self.face_edges[face_edge] = true

      if first_face_edge == nil then
         first_face_edge = face_edge
      end
      
      prev_vertex = vertex
      prev_face_edge = face_edge
   end
   
   -- Tie first and last face edges together
   first_face_edge.face_prev = prev_face_edge
   prev_face_edge.face_next = first_face_edge
   
   face.face_edge = first_face_edge
   self.faces[face] = true
   return face
end

local function detach_face_edge_from_edge(face_edge, edge)
   local only_one = face_edge.radial_next == face_edge
   if edge.face_edge == face_edge then
      if only_one then
         edge.face_edge = nil
      else
         edge.face_edge = face_edge.radial_next
      end
   end
   if not only_one then
      local prev_radial_face_edge = face_edge.radial_prev
      local next_radial_face_edge = face_edge.radial_next
      
      prev_radial_face_edge.radial_next = next_radial_face_edge
      next_radial_face_edge.radial_prev = prev_radial_face_edge
   end
   face_edge.edge = nil
end

function Mesh:remove_face(face)
   assert(self:has_face(face), "Expected to own face")
   local face_edge = face.face_edge
   repeat
      local edge = face_edge.edge
      local only_one = face_edge.radial_next == face_edge
      if edge.face_edge == face_edge then
         if only_one then
            edge.face_edge = nil
         else
            edge.face_edge = face_edge.radial_next
         end
      end
      if not only_one then
         local prev_radial_face_edge = face_edge.radial_prev
         local next_radial_face_edge = face_edge.radial_next
         
         prev_radial_face_edge.radial_next = next_radial_face_edge
         next_radial_face_edge.radial_prev = prev_radial_face_edge
      end
      self.face_edges[face_edge] = nil
      
      face_edge = face_edge.face_next
   until face_edge == face.face_edge
   
   self.faces[face] = nil
end

local function detach_edge_from_vertex(edge, vertex)
   local edge_keys = edge_keys_from(edge, vertex)
   local only_one = vertex.edge[edge_keys.next] == vertex.edge
   if vertex.edge == edge then
      if only_one then
         vertex.edge = nil
      else
         vertex.edge = edge[edge_keys.next]
      end
   end
   if not only_one then
      local prev_edge = edge:prev(vertex)
      local next_edge = edge:next(vertex)
      local prev_edge_keys = edge_keys_from(prev_edge, vertex)
      local next_edge_keys = edge_keys_from(next_edge, vertex)
      
      next_edge[next_edge_keys.prev] = prev_edge
      prev_edge[prev_edge_keys.next] = next_edge
   end
   edge[edge_keys.vertex] = nil
end

function Mesh:remove_edge(edge)
   assert(self:has_edge(edge), "Expected to own edge")
   -- Remove all faces
   local face_edge = edge.face_edge
   while edge.face_edge ~= nil do
      self:remove_face(edge.face_edge.face)
   end
   
   -- Detach from vertices
   detach_edge_from_vertex(edge, edge.v1)
   detach_edge_from_vertex(edge, edge.v2)
   self.edges[edge] = nil
end


function Mesh:remove_vertex(vertex)
   Vertex:assert_object(vertex)
   while vertex.edge ~= nil do
      self:remove_edge(vertex.edge)
   end
   self.vertices[vertex] = nil
end

function Mesh:split_edge_make_vert(edge, vertex_to_copy)
   assert(self:has_edge(edge), "Expected to own edge")
   Vertex:assert_object(vertex_to_copy)
   local new_vertex = self:add_vertex(vertex_to_copy)
   local v1 = edge.v1
   local v2 = edge.v2
   detach_edge_from_vertex(edge, v2)
   attach_edge_to_vertex(edge, new_vertex, 2)
   local new_edge = self:add_edge(new_vertex, v2)
   
   -- Update FaceEdges
   local face_edge = edge.face_edge
   if face_edge ~= nil then
      repeat
         local new_face_edge = FaceEdge:new()
         local prev_face_edge = nil
         local next_face_edge = nil
         if face_edge.vertex == v1 then
            -- The face_edge is going with the edge so, we just
            -- need to add the new_face_edge_after it
            new_face_edge.vertex = new_vertex
            prev_face_edge = face_edge
            next_face_edge = face_edge.face_next
         else
            -- The face_edge is going against the edge, so we change
            -- its vertex to the new vertex and tie the new face_edge to
            -- v2
            face_edge.vertex = new_vertex
            new_face_edge.vertex = v2
            prev_face_edge = face_edge.face_prev   
            next_face_edge = face_edge
         end
         attach_face_edge_to_edge(new_face_edge, new_edge)

         -- Add new_face_edge after original_face_edge
         new_face_edge.face_next = next_face_edge
         new_face_edge.face_prev = prev_face_edge
         next_face_edge.face_prev = new_face_edge
         prev_face_edge.face_next = new_face_edge

         self.face_edges[new_face_edge] = true
         face_edge = face_edge.radial_next
      until face_edge == edge.face_edge
   end
   
   return new_vertex
end

function Mesh:join_edge_kill_vert(edge_to_kill, vertex_to_kill)
   assert(self:has_edge(edge_to_kill), "Expected to own edge_to_kill")
   assert(self:has_vertex(vertex_to_kill), "Expected to own vertex_to_kill")
   assert(edge_to_kill:has_vertex(vertex_to_kill), "Expected edge_to_kill to be attached to vertex_to_kill")
   assert(vertex_to_kill:edge_count() == 2, "Expected vertex_to_kill to have exactly two edges")
   local to_vertex = edge_to_kill:other_vertex(vertex_to_kill)
   local other_edge = edge_to_kill:next(vertex_to_kill)
   local from_vertex = other_edge:other_vertex(vertex_to_kill)
   local other_edge_kill_vertex_index = other_edge:get_index(vertex_to_kill)
   detach_edge_from_vertex(edge_to_kill, vertex_to_kill)
   detach_edge_from_vertex(other_edge, vertex_to_kill)
   
   attach_edge_to_vertex(other_edge, to_vertex, other_edge_kill_vertex_index)
   
   -- Remove all face_edges that are tide to the edge_to_kill
   local face_edge = edge_to_kill.face_edge
   if face_edge ~= nil then
      repeat
         local prev_face_edge = face_edge.face_prev
         local next_face_edge = face_edge.face_next
         prev_face_edge.face_next = next_face_edge
         next_face_edge.face_prev = prev_face_edge
         self.face_edges[face_edge] = nil
         
         face_edge = face_edge.radial_next
      until face_edge == edge_to_kill.face_edge
   end
   
   -- Adjust vertex in face_edges that are tied to other_edge and
   -- point to vertex_to_kill
   face_edge = other_edge.face_edge
   if face_edge ~= nil then
      repeat
         if face_edge.vertex == vertex_to_kill then
            face_edge.vertex = to_vertex
         end
         face_edge = face_edge.radial_next
      until face_edge == other_edge.face_edge
   end
   self.edges[edge_to_kill] = nil
   self.vertices[vertex_to_kill] = nil
   return other_edge
end

function Mesh:split_face_make_edge(face_edge_a, face_edge_b)
   assert(self:has_face_edge(face_edge_a), "Expected to own face_edge_a")
   assert(self:has_face_edge(face_edge_b), "Expected to own face_edge_b")
   assert(face_edge_a ~= face_edge_b, "Expected face_edge_a and face_edge_b to be different")
   assert(face_edge_a.face == face_edge_b.face, "Expected face_edge_a and face_edge_b to be on the same face")
   assert(face_edge_a.vertex:find_edge_with(face_edge_b.vertex) == nil, "Expected face_edge_a and face_edge_b to be unconnected")
   local new_edge = self:add_edge(face_edge_a.vertex, face_edge_b.vertex)
   local new_face_edge_a = FaceEdge:new()
   local new_face_edge_b = FaceEdge:new()
   local new_face = Face:new()
   
   -- Secure the face to a face edge that we know will still exist.
   face_edge_a.face.face_edge = face_edge_a
   
   
   -- Move existing face_edges over to the new_face
   local face_edge = face_edge_b
   repeat
      face_edge.face = new_face
      face_edge = face_edge.face_next
   until face_edge == face_edge_a
   new_face.face_edge = face_edge_b

    -- Store temp pointers because we are going to mess everything up
   local face_edge_a_prev = face_edge_a.face_prev
   local face_edge_b_prev = face_edge_b.face_prev
   
   -- Setup new_face_edge_a
   new_face_edge_a.vertex = face_edge_b.vertex
   new_face_edge_a.face = face_edge_a.face
   attach_face_edge_to_edge(new_face_edge_a, new_edge)
   new_face_edge_a.face_next = face_edge_a
   new_face_edge_a.face_prev = face_edge_b_prev
   face_edge_a.face_prev = new_face_edge_a
   face_edge_b_prev.face_next = new_face_edge_a
   
   -- Setup new_face_edge_b
   new_face_edge_b.vertex = face_edge_a.vertex
   new_face_edge_b.face = new_face
   attach_face_edge_to_edge(new_face_edge_b, new_edge)
   new_face_edge_b.face_next = face_edge_b
   new_face_edge_b.face_prev = face_edge_a_prev
   face_edge_b.face_prev = new_face_edge_b
   face_edge_a_prev.face_next = new_face_edge_b
   
   self.faces[new_face] = true
   self.face_edges[new_face_edge_a] = true
   self.face_edges[new_face_edge_b] = true
   
   return new_face
end

function Mesh:join_face_kill_edge(face, face_to_kill, edge_to_kill)
   assert(self:has_face(face), "Expected to own face")
   assert(self:has_face(face_to_kill), "Expected to own face_to_kill")
   assert(self:has_edge(edge_to_kill), "Expected to own edge_to_kill")
   assert(face ~= face_to_kill, "Expected face and face_to_kill to be different")
   assert(face:has_edge(edge_to_kill), "Expected face to contain edge_to_kill")
   assert(face_to_kill:has_edge(edge_to_kill), "Expected face_to_kill to contain edge_to_kill")
   assert(edge_to_kill:face_count() == 2, "Expected edge_to_kill to have only two faces")
   assert(face:shared_edge_count(face_to_kill) == 1, "Expected faces to share only one edge")
   
   local kill_face_edge = edge_to_kill:find_face_edge(face_to_kill)
   -- We are also going to kill keep_face_edge, it just the face_edge
   -- tied to the face we will keep.
   local keep_face_edge = edge_to_kill:find_face_edge(face) 
   
   -- Move the face.face_edge to a safe place
   if face.face_edge == keep_face_edge then
      face.face_edge = keep_face_edge.face_next
   end
   
   -- Check if the windings on the faces match
   if kill_face_edge.vertex == keep_face_edge.vertex then
      -- The winding don't match. They are facing different directions. 
      -- Reverse the face_to_kill
      face_to_kill:reverse()
   end
   
   -- Give face_edges from face_to_kill to face
   local face_edge = face_to_kill.face_edge
   repeat
      face_edge.face = face
      face_edge = face_edge.face_next
   until face_edge == face_to_kill.face_edge
   
   self.faces[face_to_kill] = nil
   
   -- Detach face_edges around edge_to_kill and glue the two side 
   -- face_edges together
   keep_face_edge.face_next.face_prev = kill_face_edge.face_prev
   kill_face_edge.face_prev.face_next = keep_face_edge.face_next
   
   keep_face_edge.face_prev.face_next = kill_face_edge.face_next
   kill_face_edge.face_next.face_prev = keep_face_edge.face_prev
   
   self.face_edges[keep_face_edge] = nil
   self.face_edges[kill_face_edge] = nil
   
   -- Remove edge_to_kill from vertices
   detach_edge_from_vertex(edge_to_kill, edge_to_kill.v1)
   detach_edge_from_vertex(edge_to_kill, edge_to_kill.v2)
   
   self.edges[edge_to_kill] = nil
   
end

function Mesh:select_all(geometry_set)
   geometry_set = GeometrySet:use_or_new(geometry_set)
   for k, v in pairs(self.vertices) do
      if v then
         geometry_set.vertices[k] = true
      end
   end
   for k, v in pairs(self.edges) do
      if v then
         geometry_set.edges[k] = true
      end
   end
   for k, v in pairs(self.faces) do
      if v then
         geometry_set.faces[k] = true
      end
   end
   return geometry_set
end

function Mesh:create_vertex_face_data()
   local vertices = {}
   local face_indices = {}
   local vertex_inv_lookup = {}
   for k, v in pairs(self.vertices) do
      if v == true then
         table.insert(vertices, k)
         vertex_inv_lookup[k] = #vertices
      end
   end
   for k, v in pairs(self.faces) do
      if v == true then
         local single_face_indices = {}
         local face_edge = k.face_edge
         repeat
            local vertex = face_edge.vertex
            local index = vertex_inv_lookup[vertex]
            table.insert(single_face_indices, index)
            face_edge = face_edge.face_next
         until face_edge == k.face_edge
         table.insert(face_indices, single_face_indices)
      end
   end
   return {
      vertices = vertices,
      face_indices = face_indices
   }
end

function Mesh:export_wavefront_obj(filename)
   local fh = io.open(filename, 'w')
   local f = string.format
   local data = self:create_vertex_face_data()
   fh:write("# Vertices\n")
   for i, vertex in ipairs(data.vertices) do
      local v = vertex.position
      fh:write(f("v %f %f %f\n", v.x, v.y, v.z))
   end
   fh:write("\n# Normals\n")
   for i, vertex in ipairs(data.vertices) do
      local v = vertex.normal
      fh:write(f("vn %f %f %f\n", v.x, v.y, v.z))
   end
   fh:write("\n# Texture UVs\n")
   for i, vertex in ipairs(data.vertices) do
      local v = vertex.uv
      fh:write(f("vt %f %f\n", v.x, v.y))
   end

   fh:write("\n# Triangle Indices\n")
   for i, face in ipairs(data.face_indices) do
      local face_str = ""
      for k, index in ipairs(face) do
         local function make_index_string(index)
            return f(" %d/%d/%d", index, index, index)
         end
         face_str = face_str .. make_index_string(index)
      end
      fh:write(f("f %s\n", face_str ))
   end
   fh:write("\n")
   fh:close()
end


local MeshBuilder = make_class("MeshBuilder")
function MeshBuilder:new(mesh)
   Mesh:assert_object(mesh)
   local mb = {
      mesh = mesh,
      vertices = {},
   }
   self:_set_object(mb)
   return mb
end

function MeshBuilder:size()
   return #self.vertices
end

function MeshBuilder:next_index()
   return #self.vertices + 1
end

function MeshBuilder:last_index()
   return #self.vertices
end

function MeshBuilder:is_valid_index(index)
   assert(type(index) == 'number', "Expected index to be a number")
   return index >= 1 and index <= self:size()
end


function MeshBuilder:index_from_top(index)
   assert(index >= 1, "Index is less than one")
   assert(self:is_valid_index(index), "Index is greater than number of vertices")
   return self:size() + 1 - index
end



-- x, y, z, nx, ny, nz, u, v
-- {x, y, z}, {nx, ny, nz}, {u, v}
function MeshBuilder:add_point(...)
   local vertex = self.mesh:add_vertex(...)
   table.insert(self.vertices, vertex)
end

function MeshBuilder:get_point(index)
   assert(type(index) == 'number', "index is not a number")
   assert(self:is_valid_index(index), "index is out of range")
   return self.vertices[index]
end


function MeshBuilder:add_face(...)
   local args = table.pack(...)
   local indices = args
   if type(args[1]) == 'table' then
      indices = args[1]
   end
   assert(#indices > 2, "Expected at least two indices")
   local l_vertices = {}
   for i, index in ipairs(indices) do
      assert(type(index) == 'number', string.format("Expected index[%d] to be a number", i))
      assert(self:is_valid_index(index), string.format("index[%d] is out of range", i))
      local vertex = self.vertices[index]
      table.insert(l_vertices, vertex)
   end
   self.mesh:add_face(l_vertices)
end

function MeshBuilder:clear()
   self.mesh:clear()
   self.vertices = {}
end

function MeshBuilder:export_wavefront_obj(filename)
   self.mesh:export_wavefront_obj(filename)
end


local build = {}

function build.triangle_strip_from_indices(builder, vert_indices)
   MeshBuilder:assert_object(builder)
   assert(type(vert_indices) == 'table', "Expected vert_indices to be a table of numbers")
   assert(#vert_indices >= 3, "Expected vert_indices to be at least 3 long")
   assert(type(vert_indices[1]) == 'number', "Expected vert_indices[1] to be a number")
   assert(type(vert_indices[2]) == 'number', "Expected vert_indices[2] to be a number")
   local odd_index = true
   for i=3,#vert_indices do
      local index0 = vert_indices[i - 2]
      local index1 = vert_indices[i - 1]
      local index2 = vert_indices[i]
      assert(type(index2) == 'number', string.format("Expected vert_indices[%d] to be a number", i))

      if odd_index then
         builder:add_face(index0, index1, index2)
      else
         builder:add_face(index1, index0, index2)
      end
      odd_index = not odd_index
   end
end

function build.triangle_fan_from_indices(builder, vert_indices)
   MeshBuilder:assert_object(builder)
   assert(type(vert_indices) == 'table', "Expected vert_indices to be a table of numbers")
   assert(#vert_indices >= 3, "Expected vert_indices to be at least 3 long")
   assert(type(vert_indices[1]) == 'number', "Expected vert_indices[1] to be a number")
   assert(type(vert_indices[2]) == 'number', "Expected vert_indices[2] to be a number")

   for i=3,#vert_indices do
      local index0 = vert_indices[1]
      local index1 = vert_indices[i - 1]
      local index2 = vert_indices[i]
      assert(type(index2) == 'number', string.format("Expected vert_indices[%d] to be a number", i))
      builder:add_face(index0, index1, index2)
   end
end

function build.quad_strip_from_indices(builder, from_index, to_index, count)
   MeshBuilder:assert_object(builder)
   assert(type(from_index) == 'number', "Expected from_index to be a number")
   assert(from_index >= 1, "Expected from_index to be greater than or equal to 1")
   assert(type(to_index) == 'number', "Expected to_index to be a number")
   assert(to_index >= 1, "Expected to_index to be greater than or equal to 1")
   assert(count == nil or type(count) == 'number', "Expected count to be nil or a number")
   
   if count == nil then
      count = 1
   end
   assert(builder:is_valid_index(from_index + count), string.format("Expected from_index + count(%d) to fit in range of vertices", count))
   assert(builder:is_valid_index(to_index + count), string.format("Expected from_index + count(%d) to fit in range of vertices", count))
   for i=1,count do
      builder:add_face(from_index, to_index, from_index + 1)
      builder:add_face(from_index + 1, to_index, to_index + 1)
      from_index = from_index + 1
      to_index = to_index + 1
   end
   return from_index, to_index
end

function build.quad_part(builder, position, uv, build_triangles, verts_per_row)
   MeshBuilder:assert_object(builder)
   assert(vec3.is_vec3(position), "Expected position to be a vector3")
   assert(vec2.is_vec2(uv), "Expected uv to be a vector2")
   assert(type(build_triangles) == 'boolean', "Expected build_triangles to be a boolean")
   assert(type(verts_per_row) == 'number', "Expected verts_per_row to be a number")
   builder:add_point(position, 0, 0, 0, uv)
   if build_triangles then
      local base_index = builder:last_index()
      local index0 = base_index
      local index1 = base_index - 1
      local index2 = base_index - verts_per_row
      local index3 = base_index - verts_per_row - 1
      builder:add_face(index0, index2, index1)
      builder:add_face(index2, index3, index0)
   end
end


-- quad
--
-- l +----------+
-- e |          |
-- n |          |
-- g |          |
-- t |          |
-- h |          |
-- _ |          |
-- d |          |
-- i |          |
-- r |          |
--   |position  |
--   *----------+
--    width_dir
function build.quad(builder, position, width_dir, length_dir)
   MeshBuilder:assert_object(builder)
   assert(vec3.is_vec3(position), "Expected position to be a vector3")
   assert(vec3.is_vec3(width_dir), "Expected width_dir to be a vector3")
   assert(vec3.is_vec3(length_dir), "Expected length_dir to be a vector3")
   local normal = vec3.cross(length_dir, width_dir):normalize()
   local length_and_width = vec3.add(length_dir, width_dir)

   builder:add_point(position,                             normal, 0.0, 0.0)
   builder:add_point(vec3.add(position, length_dir),       normal, 0.0, 1.0)
   builder:add_point(vec3.add(position, length_and_width), normal, 1.0, 1.0)
   builder:add_point(vec3.add(position, width_dir),        normal, 1.0, 0.0)
   
   local base_index = builder:index_from_top(4)
   builder:add_face(base_index, base_index + 1, base_index + 2);
   builder:add_face(base_index, base_index + 2, base_index + 3);
end


function build.triangle(builder, corner0, corner1, corner2)
   MeshBuilder:assert_object(builder)
   assert(vec3.is_vec3(corner0), "Expected position to be a vector3")
   assert(vec3.is_vec3(corner1), "Expected position to be a vector3")
   assert(vec3.is_vec3(corner2), "Expected position to be a vector3")
   local s1 = vec3.sub(corner1, corner0)
   local s2 = vec3.sub(corner2, corner0)
   local normal = vec3.cross(s1, s2):normalize()
   
   builder:add_point(corner0, normal, 0.0, 0.0)
   builder:add_point(corner1, normal, 0.0, 1.0)
   builder:add_point(corner2, normal, 1.0, 1.0)
   
   local base_index = builder:index_from_top(3)
   builder:add_face(base_index, base_index + 1, base_index + 2)
end

local primitive = {}

local up = vec3.new(0, 1, 0)
local right = vec3.new(1, 0, 0)
local forward = vec3.new(0, 0, 1)


function primitive.cube(builder, position, width, height, depth, centered)
   MeshBuilder:assert_object(builder)
   assert(vec3.is_vec3(position), "Expected position to be a vector3")
   assert(type(width) == 'number', "Expected width to be a number")
   assert(type(height) == 'number', "Expected width to be a number")
   assert(type(depth) == 'number', "Expected width to be a number")
   assert(centered == nil or type(centered) == 'boolean', "Expected centered to be a boolean")
   
   if centered == nil then
      centered = false
   end
   
   if centered then
      position.x = position.x - (width / 2.0)
      position.y = position.y - (height / 2.0)
      position.z = position.z - (depth / 2.0)
   end
   
   
   local up_dir = up * height
   local right_dir = right * width
   local forward_dir = forward * depth
   
   local near_corner = position
   local far_corner = near_corner + up_dir + right_dir + forward_dir
   
   build.quad(builder, near_corner, forward_dir, right_dir)
   build.quad(builder, near_corner, right_dir,   up_dir)
   build.quad(builder, near_corner, up_dir,      forward_dir)

   build.quad(builder, far_corner, -right_dir,   -forward_dir)
   build.quad(builder, far_corner, -up_dir,      -right_dir)
   build.quad(builder, far_corner, -forward_dir, -up_dir)
end

local Path = make_class("Path")

function Path:new(closed, points, forwards, ups, sharps, scales)
   assert(closed == nil or type(closed) == 'boolean', "Expected closed to be nil or a boolean")
   if closed == nil then
      closed = false
   end
   local p = {
      closed = closed,
      points = {},
      forwards = {},
      ups = {},
      sharps = {},
      scales = {},
   }
   self:_set_object(p)
   if points ~= nil then
      p:add(points, forwards, ups, sharps, scales)
   end
   return p
end

function Path:add(points, forwards, ups, sharps, scales)
   assert(vec3.is_vec3(points) or type(points) == 'table', "Expected points to be either a table of vec3s or a vec3")
   if vec3.is_vec3(points) then
      assert(vec3.is_vec3(forwards), "Expected forwards to be a vec3 if points is a vec3")
      assert(vec3.is_vec3(ups), "Expected ups to be a vec3 if forward is a vec3")
      assert(scales == nil or vec3.is_vec3(scales) or type(scales) == 'number', "Expected scales to be a vec3 or number if points is a vec3")
      assert(sharps == nil or type(sharps) == 'boolean', "Expected sharps to be a nil or boolean if points is a vec3")
      if scales == nil then
         scales = vec3.new(1, 1, 1)
      end
      if sharps == nil then
         sharps = false
      end
      if type(scales) == 'number' then
         scales = vec3.new(scales, scales, scales)
      end
      table.insert(self.points,   points)
      table.insert(self.forwards, forwards)
      table.insert(self.ups,      ups)
      table.insert(self.sharps,   sharps)
      table.insert(self.scales,   scales)
   else
      assert(type(ups) == 'table', "Expected ups to be a table of vec3s if points is a table")
      assert(scales == nil or type(scales) == 'table', "Expected scales to be a table of vec3s or numbers if points is a table")
      assert(sharps == nil or type(sharps) == 'table' or type(sharps) == 'boolean', "Expected sharps to be a table of booleans or a boolean if points is a table")
      assert(#points == #ups, "Expected points and ups to be the same size")
      assert(#points == #forwards, "Expected points and forwards to be the same size")
      assert(scales == nil or #points == #scales, "Expected points and scales to be the same size")
      for i=1,#points do
         local point = points[i]
         local up = ups[i]
         local forward = forwards[i]
         local sharp = false
         if type(sharps) == 'boolean' then
            sharp = sharps
         elseif type(sharps) == 'table' then
            sharp = sharps[i]
            assert(type(sharp) == 'boolean', string.format("Expected sharps[%d] to be a boolean", i))
         end
         
         local scale = vec3.new(1, 1, 1)
         if scales ~= nil then
            scale = scales[i]
         end
         assert(vec3.is_vec3(point), string.format("Expected points[%d] to be a vec3", i))
         assert(vec3.is_vec3(forward), string.format("Expected forwards[%d] to be a vec3", i))
         assert(vec3.is_vec3(up), string.format("Expected ups[%d] to be a vec3", i))
         assert(vec3.is_vec3(scale) or type(scale) == 'number', string.format("Expected scales[%d] to be a vec3 or number", i))
         if type(scale) == 'number' then
            scale = vec3.new(scale, scale, scale)
         end

         table.insert(self.points,   point)
         table.insert(self.forwards, forward)
         table.insert(self.ups,      up)
         table.insert(self.sharps,   sharp)
         table.insert(self.scales,  scale)
      end
   end
end

function Path:iter()
   local index = 0
   local size = #self.points
   return function()
      index = index + 1
      if index <= size then
         return self:get(index)
      end
      return nil
   end
end

function Path:get(index)
   assert(type(index) == 'number', "Expected index to be a number")
   assert(index >= 1 and index <= #self.points, string.format("Expected index to be between 1 and %d", #self.points))
   return self.points[index], self.forwards[index], self.ups[index], self.sharps[index], self.scales[index]
end

function Path:__len()
   return #self.points
end

local Area = make_class("Area")

function Area:new(closed, points, sharps)
   if closed == nil then
      closed = true
   end
   local a = {
      closed = closed,
      points = {},
      sharps = {}
   }
   self:_set_object(a)
   if points ~= nil then
      a:add(points, sharps)
   end
   return a
end

function Area:add(points, sharps)
   assert(type(points) == 'table' or vec2.is_vec2(points), "Expected points to be a table of vec2 or a vec2")
   if type(points) == 'table' then
      assert(sharps == nil or type(sharps) == 'table', "Expected sharps to be nil or a table of booleans if points is a table of vec2")
      for i=1,#points do
         local point = points[i]
         local sharp = true
         if sharps ~= nil and i <= #sharps then
            sharp = sharps[i]
         end
         assert(type(sharp) == 'boolean', string.format("Expected sharps[%d] to be a boolean", i))
         assert(vec2.is_vec2(point), string.format("Expected points[%d] to be a vec2", i))
         
         table.insert(self.points, vec3.new(point.x, point.y, 0))
         table.insert(self.sharps, sharp)
      end
   else
      assert(sharps == nil or type(sharps) == 'boolean', "Expected sharps to be nil or boolean if points is a vec2")
      if sharps == nil then
         sharps = true
      end
      table.insert(self.points, vec3.new(points.x, points.y, 0))
      table.insert(self.sharps, sharps)
   end
end

function Area:__len()
   return #self.points
end

function Area:get(index)
   assert(type(index) == 'number', "Expected index to be a number")
   assert(index >= 1 and index <= #self.points, string.format("Expected index to be between 1 and %d", #self.points))
   return self.points[index], self.sharps[index]
end


function Area:iter()
   local index = 0
   local size = #self.points
   return function()
      index = index + 1
      if index <= size then
         return index, self:get(index)
      end
      return nil
   end
end

function Area:next_index(index)
   assert(type(index) == 'number', "Expected index to be a number")
   assert(index >= 1 and index <= #self.points, string.format("Expected index to be between 1 and %d", #self.points))
   if index < #self.points then
      return index + 1
   end
   return 1
end

function Area:prev_index(index)
   assert(type(index) == 'number', "Expected index to be a number")
   assert(index >= 1 and index <= #self.points, string.format("Expected index to be between 1 and %d", #self.points))
   if index > 1 then
      return index - 1
   end
   return #self.points
end

function Area:new_rectangle(width, height)
   assert(type(width) == 'number', "Expected width to be a number")
   assert(type(height) == 'number', "Expected height to be a number")
   assert(width > 0, "Expected width to be positive")
   assert(height > 0, "Expected width to be positive")
   local hw = width / 2
   local hh = height / 2
   local points = {
      vec2.new(-hw, -hh),
      vec2.new(-hw,  hh),
      vec2.new( hw,  hh),
      vec2.new( hw, -hh),
      }
   local sharps = {
      true, true, true, true
   }
   return Area:new(true, points, sharps)
end

local area_z = vec3.new(0, 0, -1)

function Area:compute_normal(index)
   assert(#self.points > 1, "Need to have more than one point in the Area")
   local point, _ = self:get(index)
   local prev_index = self:prev_index(index)
   local next_index = self:next_index(index)
   local prev_point,_ = self:get(prev_index)
   local next_point,_ = self:get(next_index)
   local d1 = point - prev_point
   local d2 = next_point - point
   local diff = (d1 + d2):scale(0.5)
   local normal = vec3.cross(diff, area_z):normalize()
   return normal

end

function Area:compute_normal_of_edge_before(index)
   assert(#self.points > 1, "Need to have more than one point in the Area")
   local point, _ = self:get(index)
   local prev_index = self:prev_index(index)
   local prev_point,_ = self:get(prev_index)
   local diff = point - prev_point
   local normal = vec3.cross(diff, area_z):normalize()
   return normal
end


function Area:compute_normal_of_edge_after(index)
   assert(#self.points > 1, "Need to have more than one point in the Area")
   local point, _ = self:get(index)
   local next_index = self:next_index(index)
   local next_point,_ = self:get(next_index)
   local diff = next_point - point
   local normal = vec3.cross(diff, area_z):normalize()
   return normal
end

function Area:build_points(builder, point, forward, up, scale, normal_override)
   MeshBuilder:assert_object(builder)
   assert(vec3.is_vec3(point), "Expected point to be a vec3")
   assert(vec3.is_vec3(forward), "Expected forward to be a vec3")
   assert(vec3.is_vec3(up), "Expected up to be a vec3")
   assert(type(scale) == 'number' or vec3.is_vec3(scale), "Expected scale to be a vec3 or a number")
   assert(normal_override == nil or vec3.is_vec3(normal_override), "Expected normal_override to be nil or a vec3")
   if type(scale) == 'number' then
      scale = vec3.new(scale, scale, scale)
   end
   
   local mat_scale = mat4.new()
   mat_scale = mat_scale:scale(mat_scale, scale)
   
   local mat_rot = mat4.from_direction(forward, up)
   mat_rot:invert(mat_rot)
   local mat = mat4.new():translate(mat_rot, point)
   local mat = mat:mul(mat_scale, mat)
   local points_generated = 0
   for i, p, sharp in self:iter() do
      local tp = mat * p
      if normal_override == nil then
         if sharp then
            local n1 = self:compute_normal_of_edge_before(i)
            local n2 = self:compute_normal_of_edge_after(i)
            n1 = mat_rot * n1
            n2 = mat_rot * n2
            if i ~= 1 or self.closed then
               builder:add_point(tp, n1)
               points_generated = points_generated + 1
            end
            if i ~= #self or self.closed then
               builder:add_point(tp, n2)
               points_generated = points_generated + 1
            end
         else
            local n = self:compute_normal(i)
            n = mat_rot * n
            builder:add_point(tp, n)
            points_generated = points_generated + 1
         end
      else
         local n = mat_rot * normal_override
         builder:add_point(tp, n)
         points_generated = points_generated + 1
      end
   end
   return points_generated
end

function Area:build_face(builder, point, forward, up, scale)
   local points_generated = self:build_points(builder, point, forward, up, scale, vec3.new(0, 0, 1))
   local base_index = builder:index_from_top(points_generated)

   local vert_indices = {}
   for i=0,points_generated - 1 do
      table.insert(vert_indices, base_index + i)
   end
   build.triangle_fan_from_indices(builder, vert_indices)
end

function Area:build_triangles(builder, from_index, to_index)
   MeshBuilder:assert_object(builder)
   assert(type(from_index) == 'number', "Expected from_index to be a number")
   assert(from_index >= 1, "Expected from_index to be greater than or equal to 1")
   assert(type(to_index) == 'number', "Expected to_index to be a number")
   assert(to_index >= 1, "Expected to_index to be greater than or equal to 1")
   
   local fi = from_index
   local ti = to_index
   
   for i, p, sharp in self:iter() do
      if sharp and (i ~= 1 or self.closed) then
         fi = fi + 1
         ti = ti + 1
      end
      
      if i == #self then
         if self.closed then
            build.triangle_strip_from_indices(builder, {from_index, to_index, fi, ti})
         end
      else
         build.triangle_strip_from_indices(builder, {fi + 1, ti + 1, fi, ti})
         fi = fi + 1
         ti = ti + 1
      end
   end

end

function build.extrude_along_path(builder, area, path, start_cap, end_cap, smooth)
   MeshBuilder:assert_object(builder)
   Area:assert_object(area)
   Path:assert_object(path)
   assert(start_cap == nil or type(start_cap) == 'boolean', "Expected start_cap to be a boolean")
   assert(end_cap == nil or type(end_cap) == 'boolean', "Expected end_cap to be a boolean")
   assert(smooth == nil or type(smooth) == 'boolean', "Expected smooth to be a boolean")
   assert(#area > 2, "Expected area to be at least 3 points")
   assert(#path > 1, "Expected path to be at least 2 points")
   
   if start_cap == nil then
      start_cap = not path.closed
   end
   if end_cap == nil then
      end_cap = not path.closed
   end
   if smooth == nil then
      smooth = true
   end
   
   local function build_box(l_point, l_forward, l_up, l_scale, l_build_triangles)
      local points_generated = area:build_points(builder, l_point, l_forward, l_up, l_scale)
   
      if l_build_triangles then
         local from_index = builder:index_from_top(2 * points_generated)
         local to_index = builder:index_from_top(points_generated)
         area:build_triangles(builder, from_index, to_index)
      end
   end
   local start_point, start_forward, start_up, start_sharp, start_scale = path:get(1)
   local end_point, end_forward, end_up, end_sharp, end_scale = path:get(#path)
      if start_cap then
      area:build_face(builder, start_point, -start_forward, start_up, start_scale)
   end
   build_box(start_point, start_forward, start_up, start_scale, false)
   for i=2,#path - 1 do
      local point, forward, up, sharp, scale = path:get(i)
      build_box(point, forward, up, scale, true)
      if sharp then
         build_box(point, forward, up, scale, false)
      end
   end

   build_box(end_point, end_forward, end_up, end_scale, true)
   if end_cap then
      area:build_face(builder, end_point, end_forward, end_up, end_scale)
   end

   
end

local CuboidBuilder = make_class("CuboidBuilder")

--- new creates a new CubodBuilder
-- This class creates a "tube" of cube parts.
-- @param start_cap If true, a quad will be added to the start.
-- @param end_cap If true, a quad will be added to the end.
-- @param[opt] width The default width of each point
-- @param[opt] height The default height of each point
-- @param[opt] smooth_side If true, the normals on the sides will be smoothed.
--                         The default value is false
function CuboidBuilder:new(start_cap, end_cap, width, height, smooth_side)
   assert(type(start_cap) == 'boolean', "Expected start_cap to be a boolean")
   assert(type(end_cap) == 'boolean', "Expected end_cap to be a boolean")
   assert(start_normal == nil or vec3.is_vec3(start_normal), "Expected start_normal to be a vector3 or nil")
   assert(end_normal == nil or vec3.is_vec3(end_normal), "Expected end_normal to be a vector3 or nil")
   assert(width == nil or type(width) == 'number', "Expected width to be a number or nil")
   assert(height == nil or type(height) == 'number', "Expected height to be a number or nil")
   assert(smooth_side == nil or type(smooth_side) == 'boolean', "Expected smooth_side to be a boolean")
   
   if smooth_side == nil then
      smooth_side = false
   end
   local cb = {
      start_cap = start_cap,
      end_cap = end_cap,
      centered = centered,
      width = width,
      height = height,
      smooth_side = smooth_side,
      points = {}
   }
   self:_set_object(cb)
   return cb
end

--- add_point adds a point
-- Adds a point to the list to be processed
-- @param point The position of the next point to add
-- @param[opt] width The width of the point. If not specified, the width
--                   specified in the constructor will be used.
-- @param[opt] height The height of the point. If not specified, the height
--                    specified in the constructor will be used.
-- @param[opt] normal The normal of the box. If not specified one will be
--                    inferred from surrounding points
-- @param[opt] smooth_side If true, the normals on the sides will be smoothed. 
--                         If not specified, the smooth_side specified in the 
--                         constructor will be used.

function CuboidBuilder:add_point(point, width, height, normal, smooth_side)
   assert(vec3.is_vec3(point), "Expected point to be a vector3")
   assert(normal == nil or vec3.is_vec3(normal), "Expected normal to be a vector3 or nil")
   assert(width == nil or type(width) == 'number', "Expected width to be a number or nil")
   assert(height == nil or type(height) == 'number', "Expected height to be a number or nil")
   assert(normal == nil or vec3.is_vec3(normal), "Expected normal to be a vector3 or nil")
   assert(not (width == nil and self.width == nil), "Expected width to be either specified in constructor or here")
   assert(not (height == nil and self.height == nil), "Expected width to be either specified in constructor or here")
   assert(smooth_side == nil or type(smooth_side) == 'boolean', "Expected smooth_side to be a boolean")

   table.insert(self.points, {
      point = point,
      width = width,
      height = height,
      normal = normal,
      smooth_side = smooth_side
   })
end

function component_size_order(v)
   assert(vec3.is_vec3(v), "Expected v to be a vector3")
   local av = {v.x, v.y, v.z}
   local ak = {'x', 'y', 'z'}
   local out = {}
   local function findmax()
      local max_index = 1
      for i=2,#av do
         if av[max_index] < av[i] then
            max_index = i
         end
      end
      return max_index
   end
   local i1 = findmax()
   table.insert(out, ak[i1])
   table.remove(ak, i1)
   table.remove(av, i1)
   local i2 = findmax()
   table.insert(out, ak[i2])
   table.remove(ak, i1)
   table.remove(av, i1)
   table.insert(out, ak[1])
   return out
end

function CuboidBuilder:generate(builder)
   MeshBuilder:assert_object(builder)
   assert(#self.points >= 2, "Expected at least two points to be specified")

   
   local start_point = self.points[1]
   local end_point = self.points[#self.points]
   
   local start_normal = start_point.normal    
   if start_normal == nil then
      start_normal = vec3.sub(self.points[2].point, start_point.point)
   end
   start_normal = start_normal:normalize()
   
   local end_normal = end_point.normal
   if end_normal == nil then
      end_normal = vec3.sub(end_point.point, self.points[#self.points - 1].point)
   end
   end_normal = end_normal:normalize()
   
   local start_width = start_point.width or self.width
   local start_height = start_point.height or self.height
   
   local end_width = end_point.width or self.width
   local end_height = end_point.height or self.height

   local up_vec = (function()
      local size_order = component_size_order(start_normal)
      local up_vec = vec3.new(0, 0, 0)
      up_vec[size_order[3]] = 1
      return up_vec
   end)()

   local function get_rotated_points(point, width, height, normal, up_vec)
      local hw = width  / 2.0
      local hh = height / 2.0
      local p0 = vec3.new(-hw, -hh, 0)
      local p1 = vec3.new( hw, -hh, 0)
      local p2 = vec3.new( hw,  hh, 0)
      local p3 = vec3.new(-hw,  hh, 0)
      
      
      -- Compute rotation due to normal
      -- This assumes normal is normalized
      local mat_rot = mat4.from_direction(normal, up_vec)
      mat_rot:invert(mat_rot)
      local rot_axis = vec3.cross(up, normal)
      local mat = mat4.new():translate(mat_rot, point)
      p0 = mat * p0
      p1 = mat * p1
      p2 = mat * p2
      p3 = mat * p3
      return {
         p0 = p0,
         p1 = p1,
         p2 = p2,
         p3 = p3,
         mat_rot = mat_rot
      }
   end
   local function update_up_vect_from_normal(prev_normal, next_normal, up_vec)
      local dot = vec3.dot(next_normal, up_vec)
      if math.abs(dot) < 0.9 then
         return up_vec
      end
      local cross1 = vec3.cross(prev_normal, next_normal)
      local new_up_vec = vec3.cross(cross1, up_vec):normalize()
      return new_up_vec
   end
   local function build_cap(point, width, height, normal, up_vec)
      local pdata = get_rotated_points(point, width, height, normal, up_vec)
      
      builder:add_point(pdata.p0, normal)
      builder:add_point(pdata.p1, normal)
      builder:add_point(pdata.p2, normal)
      builder:add_point(pdata.p3, normal)
      
      local base_index = builder:index_from_top(4)
      
      builder:add_face(base_index + 1, base_index, base_index + 2)
      builder:add_face(base_index + 2, base_index, base_index + 3)
   end
   
   local function build_box(point, width, height, normal, up_vec, build_triangles, smooth_side)
      local pdata = get_rotated_points(point, width, height, normal, up_vec)
      local nx = vec3.new(1, 0, 0)
      local nz = vec3.new(0, 0, 1)

      -- Compute rotation due to normal
      -- This assumes normal is normalized
      local rot_axis = vec3.cross(up, normal)
      if pdata.mat_rot ~= nil then
         nx = pdata.mat_rot * nx
         nz = pdata.mat_rot * nz
      end
      
      builder:add_point(pdata.p0, -nz)
      builder:add_point(pdata.p1, -nz)
      
      builder:add_point(pdata.p1, nx)
      builder:add_point(pdata.p2, nx)
      
      builder:add_point(pdata.p2, nz)
      builder:add_point(pdata.p3, nz)
      
      builder:add_point(pdata.p3, -nx)
      builder:add_point(pdata.p0, -nx)
      
      if build_triangles then
         local new_box_index = builder:index_from_top(8)
         local prev_box_index = builder:index_from_top(16)
         local function build_quad(ni, pi)
            builder:add_face(pi, ni,     ni + 1)
            builder:add_face(pi, ni + 1, pi + 1)
         end
         build_quad(new_box_index,     prev_box_index)
         build_quad(new_box_index + 2, prev_box_index + 2)
         build_quad(new_box_index + 4, prev_box_index + 4)
         build_quad(new_box_index + 6, prev_box_index + 6)

      
         if not smooth_side then
            -- correct normals
            local function flatten_normal(ni, pi)
               local vp1 = builder:get_point(pi)
               local vp2 = builder:get_point(pi + 1)
               local vn1 = builder:get_point(ni)
               local vn2 = builder:get_point(ni)
               local v1 = vp2.position - vp1.position
               local v2 = vn1.position - vp1.position
               -- TODO: Confirm this puts the normals on the outside
               local normal = v2:cross(v1):normalize()
               
               vp1.normal = vec3.new(normal)
               vp2.normal = vec3.new(normal)
               vn1.normal = vec3.new(normal)
               vn2.normal = vec3.new(normal)
               
            end
            flatten_normal(new_box_index,     prev_box_index)
            flatten_normal(new_box_index + 2, prev_box_index + 2)
            flatten_normal(new_box_index + 4, prev_box_index + 4)
            flatten_normal(new_box_index + 6, prev_box_index + 6)  
         end
      end
   end
   local function bool_fallback(a, b)
      if a == nil then
         return b
      end
      return a
   end


   if self.start_cap then
      build_cap(start_point.point, start_width, start_height, -start_normal, up_vec)
   end

   build_box(start_point.point, start_width, start_height, start_normal, up_vec, false, bool_fallback(start_point.smooth_side, self.smooth_side))
   local prev_normal = start_normal
   for i=2,#self.points - 1 do
      local current_p = self.points[i]
      local normal = current_p.normal
      if normal == nil then
         local prev_p = self.points[i - 1]
         local next_p = self.points[i + 1]
         -- Compute Normal
         local v1 = vec3.sub(current_p.point, prev_p.point)
         local v2 = vec3.sub(next_p.point, current_p.point)
         normal = (v1 + v2):scale(0.5)
      end
      normal = normal:normalize()
      up_vec = update_up_vect_from_normal(prev_normal, normal, up_vec)
      local width = current_p.width or self.width
      local height = current_p.height or self.height
      local smooth_side = bool_fallback(current_p.smooth_side, self.smooth_side)
      -- Build the box
      build_box(current_p.point, width, height, normal, up_vec, true, smooth_side)
      if not smooth_side then
         build_box(current_p.point, width, height, normal, up_vec, false, smooth_sideaa)
      end
      prev_normal = normal
   end
   
   up_vec = update_up_vect_from_normal(prev_normal, end_normal, up_vec)
   build_box(end_point.point, end_width, end_height, end_normal, up_vec, true, bool_fallback(end_point.smooth_side, self.smooth_side))
   if self.end_cap then
      build_cap(end_point.point, end_width, end_height, end_normal, up_vec)
   end

end

return {
   Mesh = Mesh,
   GeometrySet = GeometrySet,
   Vertex = Vertex,
   Edge = Edge,
   FaceEdge = FaceEdge,
   Face = Face,
   MeshBuilder = MeshBuilder,
   build = build,
   primitive = primitive,
   Path = Path,
   Area = Area,
   CuboidBuilder = CuboidBuilder
}

