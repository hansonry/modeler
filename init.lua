local modules = (...) and (...):gsub('%.init$', '') .. ".modules." or ""

local modeler = {
   _LICENSE = "modeler is distributed under the terms of the MIT license. See LICENSE.",
   _URL = "https://gitlab.com/hansonry/modeler",
   _VERSION = "0.0.1",
   _DESCRIPTION = "A library for creating 3d art assets programmatically"
}

local files = {
   "modeler",
}

for _, file in ipairs(files) do
   modeler[file] = require(modules .. file)
end

return modeler
