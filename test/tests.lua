#!/usr/bin/env lua


local lu = require('test.luaunit')
local m  = require('.')
local Mesh = m.modeler.Mesh




TestFace = {} --class

   function TestFace:setUp()
      -- set up tests
   end

   function TestFace:test_creation_default()
      local mesh = Mesh:new()
      local v1 = mesh:add_vertex(0, 0, 0)
      local v2 = mesh:add_vertex(0, 1, 0)
      local v3 = mesh:add_vertex(1, 1, 0)
      local v4 = mesh:add_vertex(1, 0, 0)
      local face = mesh:add_face(v1, v2, v3, v4)
      local normal = face:compute_normal()
      lu.assertEquals( normal.x ,  0 )
      lu.assertEquals( normal.y ,  0 )
      lu.assertEquals( normal.z , -1 )
   end


local runner = lu.LuaUnit.new()
runner:setOutputType("text")
os.exit( runner:runSuite() )