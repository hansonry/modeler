#!/usr/bin/env lua

local m = require('.').modeler
local dbg = require('debugger')

local cpml = require('cpml')
local vec3 = cpml.vec3
local vec2 = cpml.vec2


local Mesh = m.Mesh
local Vertex = m.Vertex



local function ap(p, x, y, z)
   assert(vec3.is_vec3(p), "Expected p to be a vec3")
   assert(type(x) == 'number', "Expected x to be a number")
   assert(type(y) == 'number', "Expected y to be a number")
   assert(type(z) == 'number', "Expected z to be a number")
   return p + vec3.new(x, y, z)
end


(function()
   print("test_mesh_split_edge_make_vert")
   local mesh = Mesh:new();
   local z = 0;
   
   (function(p)
      print("   Single Edge")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 1, 0, 0))
      local edge = mesh:add_edge(v1, v2)
      local nv = v1:lerp(v2, 0.5)
      mesh:split_edge_make_vert(edge, nv)
   end)(vec3.new(0, 0, z));

   z = z + 1;
   (function(p)
      print("   On Face")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v3 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 0, 0))
      local edge = mesh:add_edge(v1, v2)
      mesh:add_face(v1, v2, v3, v4)
      local nv = v1:lerp(v2, 0.5)
      mesh:split_edge_make_vert(edge, nv)
   end)(vec3.new(0, 0, z));
   

   z = z + 1;
   (function(p)
      print("   Between 2 Faces")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v3 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 0, 0))
      local v5 = mesh:add_vertex(ap(p, 2, 1, 0))
      local v6 = mesh:add_vertex(ap(p, 2, 0, 0))
      local edge = mesh:add_edge(v3, v4)
      mesh:add_face(v1, v2, v3, v4)
      mesh:add_face(v5, v6, v4, v3)
      local nv = v3:lerp(v4, 0.5)
      mesh:split_edge_make_vert(edge, nv)
   end)(vec3.new(0, 0, z));

   z = z + 2;
   (function(p)
      print("   Between 4 Faces")
      local v1  = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2  = mesh:add_vertex(ap(p, 0, 1, 0))
      local v3  = mesh:add_vertex(ap(p, 1, 1, 0))
      local v4  = mesh:add_vertex(ap(p, 1, 0, 0))
      local v5  = mesh:add_vertex(ap(p, 2, 0, 0))
      local v6  = mesh:add_vertex(ap(p, 2, 1, 0))
      local v7  = mesh:add_vertex(ap(p, 1, 0, 1))
      local v8  = mesh:add_vertex(ap(p, 1, 1, 1))
      local v9  = mesh:add_vertex(ap(p, 1, 0, -1))
      local v10 = mesh:add_vertex(ap(p, 1, 1, -1))
      local edge = mesh:add_edge(v3, v4)
      mesh:add_face(v1, v2, v3, v4)
      mesh:add_face(v5, v6, v3, v4)
      mesh:add_face(v7, v8, v3, v4)
      mesh:add_face(v9, v10, v3, v4)
      local nv = v3:lerp(v4, 0.5)
      mesh:split_edge_make_vert(edge, nv)
   end)(vec3.new(0, 0, z));

   z = z + 2;
   (function(p)
      print("   Between 4 Faces 3 times")
      local v1  = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2  = mesh:add_vertex(ap(p, 0, 1, 0))
      local v3  = mesh:add_vertex(ap(p, 1, 1, 0))
      local v4  = mesh:add_vertex(ap(p, 1, 0, 0))
      local v5  = mesh:add_vertex(ap(p, 2, 0, 0))
      local v6  = mesh:add_vertex(ap(p, 2, 1, 0))
      local v7  = mesh:add_vertex(ap(p, 1, 0, 1))
      local v8  = mesh:add_vertex(ap(p, 1, 1, 1))
      local v9  = mesh:add_vertex(ap(p, 1, 0, -1))
      local v10 = mesh:add_vertex(ap(p, 1, 1, -1))
      local edge = mesh:add_edge(v3, v4)
      mesh:add_face(v1, v2, v3, v4)
      mesh:add_face(v5, v6, v3, v4)
      mesh:add_face(v7, v8, v3, v4)
      mesh:add_face(v9, v10, v3, v4)
      local nv = v3:lerp(v4, 0.5)
      nv = mesh:split_edge_make_vert(edge, nv)
      local other_edge = edge:next(nv)
      mesh:split_edge_make_vert(edge, nv:lerp(v3, 0.5))
      mesh:split_edge_make_vert(other_edge, nv:lerp(v4, 0.5))
   end)(vec3.new(0, 0, z));
   
   
   
   mesh:export_wavefront_obj("test_mesh_split_edge_make_vert.obj")
end)();


(function()
   print("test_join_edge_kill_vert")
   local mesh = Mesh:new();
   local z = 0;
   
   (function(p)
      print("   No Face")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 1, 0, 0))
      local v3 = mesh:add_vertex(ap(p, 2, 0, 0))
      local edge_to_kill = mesh:add_edge(v1, v2)
      local other_edge = mesh:add_edge(v2, v3)
      
      mesh:join_edge_kill_vert(edge_to_kill, v2)
      assert(not mesh:has_vertex(v2), "Expected Vertex to be removed")
      assert(not mesh:has_edge(edge_to_kill), "Expected Edge to be removed")
      assert(mesh:has_edge(other_edge), "Expected the other edge to be left alone")
   end)(vec3.new(0, 0, z));
   z = z + 1;
   
   (function(p)
      print("   1 Face")
      local v1 = mesh:add_vertex(ap(p, 0, 0,   0))
      local v2 = mesh:add_vertex(ap(p, 0, 1,   0))
      local v3 = mesh:add_vertex(ap(p, 1, 1,   0))
      local v4 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0,   0))
      
      local edge_to_kill = mesh:add_edge(v4, v5)
      mesh:add_face(v1, v2, v3, v4, v5)
      
      mesh:join_edge_kill_vert(edge_to_kill, v4)
   end)(vec3.new(0, 0, z));
   
   z = z + 2;
   (function(p)
      print("   4 Faces")
      local v1  = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2  = mesh:add_vertex(ap(p, 0, 1, 0))
      local v3  = mesh:add_vertex(ap(p, 1, 1, 0))
      local v4  = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v5  = mesh:add_vertex(ap(p, 1, 0, 0))
      local v6  = mesh:add_vertex(ap(p, 2, 0, 0))
      local v7  = mesh:add_vertex(ap(p, 2, 1, 0))
      local v8  = mesh:add_vertex(ap(p, 1, 0, 1))
      local v9  = mesh:add_vertex(ap(p, 1, 1, 1))
      local v10 = mesh:add_vertex(ap(p, 1, 0, -1))
      local v11 = mesh:add_vertex(ap(p, 1, 1, -1))
      local edge_to_kill = mesh:add_edge(v5, v4)
      mesh:add_face(v1,  v2,  v3, v4, v5)
      mesh:add_face(v6,  v7,  v3, v4, v5)
      mesh:add_face(v8,  v9,  v3, v4, v5)
      mesh:add_face(v10, v11, v3, v4, v5)

      mesh:join_edge_kill_vert(edge_to_kill, v4)
   end)(vec3.new(0, 0, z));

   z = z + 2;
   (function(p)
      print("   4 Faces Twice")
      local v1  = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2  = mesh:add_vertex(ap(p, 0, 1, 0))
      local v3  = mesh:add_vertex(ap(p, 1, 1, 0))
      local v4  = mesh:add_vertex(ap(p, 1, 0.66, 0))
      local v5  = mesh:add_vertex(ap(p, 1, 0.33, 0))
      local v6  = mesh:add_vertex(ap(p, 1, 0, 0))
      local v7  = mesh:add_vertex(ap(p, 2, 0, 0))
      local v8  = mesh:add_vertex(ap(p, 2, 1, 0))
      local v9  = mesh:add_vertex(ap(p, 1, 0, 1))
      local v10  = mesh:add_vertex(ap(p, 1, 1, 1))
      local v11 = mesh:add_vertex(ap(p, 1, 0, -1))
      local v12 = mesh:add_vertex(ap(p, 1, 1, -1))
      local edge_to_kill1 = mesh:add_edge(v3, v4)
      local edge_to_kill2 = mesh:add_edge(v4, v5)
      mesh:add_face(v1,  v2,  v3, v4, v5, v6)
      mesh:add_face(v7,  v8,  v3, v4, v5, v6)
      mesh:add_face(v9,  v10, v3, v4, v5, v6)
      mesh:add_face(v11, v12, v3, v4, v5, v6)

      mesh:join_edge_kill_vert(edge_to_kill1, v4)
      mesh:join_edge_kill_vert(edge_to_kill2, v5)
   end)(vec3.new(0, 0, z));
   
   mesh:export_wavefront_obj("test_join_edge_kill_vert.obj")
end)();


(function()
   print("test_split_face_make_edge")
   local mesh = Mesh:new();
   local z = 0;
   
   (function(p)
      print("   Single Middle")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 0.5, 0))
      local v3 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v6 = mesh:add_vertex(ap(p, 1, 0, 0))
      local face = mesh:add_face(v1, v2, v3, v4, v5, v6)
      mesh:split_face_make_edge(face:find_face_edge(v2), face:find_face_edge(v5))
   end)(vec3.new(0, 0, z));
   
   z = z + 1;
   (function(p)
      print("   Three Split")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 0.5, 0))
      local v3 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v6 = mesh:add_vertex(ap(p, 1, 0, 0))
      local face1 = mesh:add_face(v1, v2, v3, v4, v5, v6)
      local face2 = mesh:split_face_make_edge(face1:find_face_edge(v2), face1:find_face_edge(v5))
      mesh:split_face_make_edge(face1:find_face_edge(v3), face1:find_face_edge(v5))
      mesh:split_face_make_edge(face2:find_face_edge(v1), face2:find_face_edge(v5))
   end)(vec3.new(0, 0, z));

   mesh:export_wavefront_obj("test_split_face_make_edge.obj")
end)();

(function()
   print("test_face_reverse")
   local mesh = Mesh:new();
   local z = 0;
   
   (function(p)
      print("   Single Reverse")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 0.5, 0))
      local v3 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v6 = mesh:add_vertex(ap(p, 1, 0, 0))
      local face = mesh:add_face(v1, v2, v3, v4, v5, v6)
      face:reverse()
   end)(vec3.new(0, 0, z));
   
   z = z + 1;
   (function(p)
      print("   Single Reverse Twice")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 0.5, 0))
      local v3 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v6 = mesh:add_vertex(ap(p, 1, 0, 0))
      local face = mesh:add_face(v1, v2, v3, v4, v5, v6)
      face:reverse()
      face:reverse()
   end)(vec3.new(0, 0, z));

   mesh:export_wavefront_obj("test_face_reverse.obj")
end)();


(function()
   print("test_join_face_kill_edge")
   local mesh = Mesh:new();
   local z = 0;
   
   (function(p)
      print("   One Edge Killed")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 0.5, 0))
      local v3 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v6 = mesh:add_vertex(ap(p, 1, 0, 0))
      local edge_to_kill = mesh:add_edge(v2, v5)
      local face         = mesh:add_face(v1, v2, v5, v6)
      local face_to_kill = mesh:add_face(v2, v3, v4, v5)
      mesh:join_face_kill_edge(face, face_to_kill, edge_to_kill)
   end)(vec3.new(0, 0, z));
   z = z + 1;
   (function(p)
      print("   Three Edges Killed")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 0.5, 0))
      local v3 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v6 = mesh:add_vertex(ap(p, 1, 0, 0))
      local edge_to_kill1 = mesh:add_edge(v2, v5)
      local edge_to_kill2 = mesh:add_edge(v3, v5)
      local edge_to_kill3 = mesh:add_edge(v1, v5)
      local face          = mesh:add_face(v1, v2, v5)
      local face_to_kill1 = mesh:add_face(v2, v3, v5)
      local face_to_kill2 = mesh:add_face(v3, v4, v5)
      local face_to_kill3 = mesh:add_face(v1, v5, v6)
      mesh:join_face_kill_edge(face, face_to_kill1, edge_to_kill1)
      mesh:join_face_kill_edge(face, face_to_kill2, edge_to_kill2)
      mesh:join_face_kill_edge(face, face_to_kill3, edge_to_kill3)
   end)(vec3.new(0, 0, z));
   z = z + 1;
   (function(p)
      print("   Three Edges Killed With Flips")
      local v1 = mesh:add_vertex(ap(p, 0, 0, 0))
      local v2 = mesh:add_vertex(ap(p, 0, 0.5, 0))
      local v3 = mesh:add_vertex(ap(p, 0, 1, 0))
      local v4 = mesh:add_vertex(ap(p, 1, 1, 0))
      local v5 = mesh:add_vertex(ap(p, 1, 0.5, 0))
      local v6 = mesh:add_vertex(ap(p, 1, 0, 0))
      local edge_to_kill1 = mesh:add_edge(v2, v5)
      local edge_to_kill2 = mesh:add_edge(v3, v5)
      local edge_to_kill3 = mesh:add_edge(v1, v5)
      local face          = mesh:add_face(v1, v2, v5)
      local face_to_kill1 = mesh:add_face(v2, v5, v3)
      local face_to_kill2 = mesh:add_face(v3, v4, v5)
      local face_to_kill3 = mesh:add_face(v1, v6, v5)
      mesh:join_face_kill_edge(face, face_to_kill1, edge_to_kill1)
      mesh:join_face_kill_edge(face, face_to_kill2, edge_to_kill2)
      mesh:join_face_kill_edge(face, face_to_kill3, edge_to_kill3)
   end)(vec3.new(0, 0, z));
   

   mesh:export_wavefront_obj("test_join_face_kill_edge.obj")
end)();
