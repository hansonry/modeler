# Modeler

Implementing 
[Modeling by Numbers](https://jayelinda.com/modelling-by-numbers-part-1a/)
and 
[Blenders BMesh](https://developer.blender.org/docs/features/objects/mesh/bmesh/) 
in Lua.
