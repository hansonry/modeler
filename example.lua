#!/usr/bin/env lua
local m = require('.').modeler
local dbg = require('debugger')

local cpml = require('cpml')
local vec3 = cpml.vec3
local vec2 = cpml.vec2


local MeshBuilder = m.MeshBuilder
local build = m.build
local primitive = m.primitive
local CuboidBuilder = m.CuboidBuilder
local Area = m.Area
local Path = m.Path
local Mesh = m.Mesh

local mesh = Mesh:new()
local mb = MeshBuilder:new(mesh)
mb:add_point(0, 0, 0)
mb:add_point(1, 0, 0)
mb:add_point(1, 1, 0)
mb:add_face(1, 2, 3)
mb:export_wavefront_obj("builder_triangle.obj")


mb:clear()
build.quad(mb, vec3.new(0, 0, 0), vec3.new(0, 1, 0), vec3.new(1, 0, 0))
mb:export_wavefront_obj("quad.obj")


mb:clear()
primitive.cube(mb, vec3.new(0, 0, 0), 1, 2, 3, true)
mb:export_wavefront_obj("cube.obj")

mb:clear()
build.triangle(mb, vec3.new(0, 0, 0), vec3.new(1, 0, 0), vec3.new(1, 1, 0))
mb:export_wavefront_obj("triangle.obj")

mb:clear()
local area = Area:new_rectangle(1, 1)
local path = Path:new()
path:add(vec3.new(0, 0, 0), vec3.new(0, 1, 0), vec3.new(0, 0, 1))
path:add(vec3.new(0, 1, 0), vec3.new(0, 1, 0), vec3.new(0, 0, 1))
path:add(vec3.new(0.5, 2, 0), vec3.new(0, 1, 0), vec3.new(0, 0, 1))
build.extrude_along_path(mb, area, path)
mb:export_wavefront_obj("extrude1.obj")

mb:clear()
local cb = CuboidBuilder:new(true, true, 1, 1, false)
cb:add_point(vec3.new(0, 0, 0))
cb:add_point(vec3.new(0, 1, 0))
cb:add_point(vec3.new(1, 2, 0))
cb:add_point(vec3.new(2, 2, 0))

cb:generate(mb)
mb:export_wavefront_obj("cuboid1.obj")



mb:clear()
local cb = CuboidBuilder:new(true, true, 1, 1.5, false)
cb:add_point(vec3.new(0, 0, 0))
cb:add_point(vec3.new(0, 1, 0))
cb:add_point(vec3.new(1, 2, 0))
cb:add_point(vec3.new(2, 2, 0))
cb:add_point(vec3.new(3, 2, 1))

cb:generate(mb)
mb:export_wavefront_obj("cuboid2.obj")



